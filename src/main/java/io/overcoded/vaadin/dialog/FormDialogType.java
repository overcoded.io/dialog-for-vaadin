package io.overcoded.vaadin.dialog;

import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import io.overcoded.vaadin.dialog.config.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.Duration;
import java.util.List;

@Getter
@RequiredArgsConstructor
public enum FormDialogType {
    DEFAULT(DialogConfig.builder().build()),

    DEFAULT_WITHOUT_BEAN_VALIDATION(DialogConfig.builder()
            .form(FormConfig.builder().beanValidationEnabled(false).build())
            .build()),

    COMPACT(DialogConfig.builder()
            .title(TitleConfig.builder().enabled(false).build())
            .form(FormConfig.builder()
                    .beanValidationEnabled(true)
                    .saveAction(FormButtonConfig.builder()
                            .enabled(true)
                            .label("Save")
                            .closeOnSuccess(true)
                            .notification(NotificationConfig.builder()
                                    .enabled(true)
                                    .duration(Duration.ofSeconds(10))
                                    .position(Notification.Position.TOP_END)
                                    .build())
                            .iconFactory(VaadinIcon.PENCIL)
                            .variants(List.of(ButtonVariant.LUMO_PRIMARY))
                            .successMessage("Dialog content has been saved.")
                            .errorMessage("Failed to save dialog. Please contact your system administrator.")
                            .build())
                    .cancelAction(FormButtonConfig.builder()
                            .enabled(true)
                            .label("Cancel")
                            .closeOnSuccess(true)
                            .notification(NotificationConfig.builder()
                                    .enabled(true)
                                    .duration(Duration.ofSeconds(10))
                                    .position(Notification.Position.TOP_END)
                                    .build())
                            .iconFactory(VaadinIcon.CLOSE)
                            .variants(List.of(ButtonVariant.LUMO_CONTRAST))
                            .errorMessage("Failed to close dialog. Please contact your system administrator.")
                            .build())
                    .secondaryAction(FormButtonConfig.builder()
                            .enabled(true)
                            .label("Reset")
                            .closeOnSuccess(false)
                            .iconFactory(VaadinIcon.REFRESH)
                            .notification(NotificationConfig.builder().enabled(false).build())
                            .variants(List.of(ButtonVariant.LUMO_ERROR))
                            .build())
                    .build())
            .build()),

    COMPACT_WITHOUT_BEAN_VALIDATION(DialogConfig.builder()
            .title(TitleConfig.builder().enabled(false).build())
            .form(FormConfig.builder()
                    .beanValidationEnabled(false)
                    .saveAction(FormButtonConfig.builder()
                            .enabled(true)
                            .label("Save")
                            .closeOnSuccess(true)
                            .notification(NotificationConfig.builder()
                                    .enabled(true)
                                    .duration(Duration.ofSeconds(10))
                                    .position(Notification.Position.TOP_END)
                                    .build())
                            .iconFactory(VaadinIcon.PENCIL)
                            .variants(List.of(ButtonVariant.LUMO_PRIMARY))
                            .successMessage("Dialog content has been saved.")
                            .errorMessage("Failed to save dialog. Please contact your system administrator.")
                            .build())
                    .cancelAction(FormButtonConfig.builder()
                            .enabled(true)
                            .label("Cancel")
                            .closeOnSuccess(true)
                            .notification(NotificationConfig.builder()
                                    .enabled(true)
                                    .duration(Duration.ofSeconds(10))
                                    .position(Notification.Position.TOP_END)
                                    .build())
                            .iconFactory(VaadinIcon.CLOSE)
                            .variants(List.of(ButtonVariant.LUMO_TERTIARY, ButtonVariant.LUMO_CONTRAST))
                            .errorMessage("Failed to close dialog. Please contact your system administrator.")
                            .build())
                    .secondaryAction(FormButtonConfig.builder()
                            .enabled(true)
                            .label("Reset")
                            .closeOnSuccess(false)
                            .iconFactory(VaadinIcon.REFRESH)
                            .notification(NotificationConfig.builder().enabled(false).build())
                            .variants(List.of(ButtonVariant.LUMO_ERROR))
                            .build())
                    .build())
            .build());

    private final DialogConfig config;
}
