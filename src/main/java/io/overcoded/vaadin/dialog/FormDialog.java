package io.overcoded.vaadin.dialog;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.IconFactory;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.theme.lumo.LumoIcon;
import io.overcoded.vaadin.dialog.config.DialogConfig;
import io.overcoded.vaadin.dialog.config.FormButtonConfig;
import io.overcoded.vaadin.dialog.config.NotificationConfig;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

@Slf4j
@Getter
@Setter
public class FormDialog<T> extends CloseableDialog {
    protected final Binder<T> binder;
    protected final Class<T> beanType;
    protected transient T value;
    protected transient Function<T, Boolean> saveFunction;
    protected transient Function<T, Boolean> cancelFunction;

    public FormDialog(Class<T> beanType, FormLayout content) {
        this(beanType, new DialogConfig(), null, null, content);
    }

    public FormDialog(Class<T> beanType, DialogConfig config, FormLayout content) {
        this(beanType, config, null, null, content);
    }

    public FormDialog(Class<T> beanType, String title) {
        this(beanType, new DialogConfig(), null, title, null);
    }

    public FormDialog(Class<T> beanType, IconFactory iconFactory, String title) {
        this(beanType, new DialogConfig(), iconFactory, title, null);
    }

    public FormDialog(Class<T> beanType, IconFactory iconFactory, String title, FormLayout content) {
        this(beanType, new DialogConfig(), iconFactory, title, content);
    }

    public FormDialog(Class<T> beanType, DialogConfig config, IconFactory iconFactory, String title, FormLayout content) {
        super(config, iconFactory, title, null);
        this.beanType = beanType;
        this.binder = config.getForm().isBeanValidationEnabled() ? new BeanValidationBinder<>(beanType) : new Binder<>(beanType);
        this.setForm(content);
        this.setFooter();
    }

    public void open(T value) {
        setValue(value);
        open();
    }

    public void setValue(T value) {
        this.value = value;
        this.binder.readBean(value);
    }

    private void setFooter() {
        createAction(config.getForm().getSecondaryAction(), e -> reset(config.getForm().getSecondaryAction())).ifPresent(button -> {
            button.getStyle().set("margin-right", "auto");
            getFooter().add(button);
        });
        createAction(config.getForm().getCancelAction(), e -> cancel(config.getForm().getCancelAction())).ifPresent(button -> getFooter().add(button));
        createAction(config.getForm().getSaveAction(), e -> save(config.getForm().getSaveAction())).ifPresent(button -> getFooter().add(button));
    }

    private Optional<Button> createAction(FormButtonConfig config, ComponentEventListener<ClickEvent<Button>> eventListener) {
        Optional<Button> result = Optional.empty();
        if (config.isEnabled()) {
            Button button = new Button();
            if (Objects.nonNull(config.getLabel()) && !config.getLabel().isBlank()) {
                button.setText(config.getLabel());
            }
            if (Objects.nonNull(config.getIconFactory())) {
                button.setIcon(config.getIconFactory().create());
            }
            button.addClickListener(eventListener);
            button.addThemeVariants(config.getVariants().toArray(new ButtonVariant[0]));
            result = Optional.of(button);
        }
        return result;
    }


    public void setForm(FormLayout layout) {
        binder.bindInstanceFields(layout);
        if (layout instanceof HasCustomValidationRules) {
            HasCustomValidationRules<T> customValidation = (HasCustomValidationRules<T>) layout;
            customValidation.registerBindings(binder);
        }
        wrapForm(layout);
    }

    @Override
    public void setContent(Component content) {
        if (Objects.nonNull(content)) {
            if (content instanceof FormLayout formLayout) {
                setForm(formLayout);
            } else {
                log.warn("Content can't be set as FormDialog content should be a FormLayout.");
            }
        }
    }

    protected void wrapForm(FormLayout content) {
        if (Objects.nonNull(content)) {
            Scroller scroller = new Scroller();
            scroller.setSizeFull();
            removeAll();

            scroller.setContent(wrapFormLayout(content));
            add(scroller);
        }
    }

    protected Component wrapFormLayout(FormLayout content) {
        HorizontalLayout layout = new HorizontalLayout();
        layout.setSizeFull();
        layout.setPadding(true);
        layout.add(content);
        layout.getStyle()
                .set("margin-top", "0")
                .set("padding-top", "0");
        return layout;
    }

    protected void reset(FormButtonConfig actionConfig) {
        binder.readBean(value);
    }

    protected void save(FormButtonConfig actionConfig) {
        try {
            createInstanceIfNotPresent();
            binder.writeBean(value);
            NotificationConfig notificationConfig = actionConfig.getNotification();
            if (doSave()) {
                if (notificationConfig.isEnabled()) {
                    showSuccessNotification(actionConfig.getSuccessMessage(), notificationConfig);
                }
                if (actionConfig.isCloseOnSuccess()) {
                    close();
                }
            } else {
                showErrorNotification(actionConfig.getErrorMessage(), notificationConfig);
            }
        } catch (ValidationException ex) {
            if (log.isDebugEnabled()) {
                log.debug("Failed to validate form selection: ", ex);
            } else {
                log.debug("Failed to validate form selection: {}", ex.getMessage());
            }
        }
    }

    protected void cancel(FormButtonConfig actionConfig) {
        if (doCancel()) {
            if (actionConfig.isCloseOnSuccess()) {
                close();
            }
        } else {
            showErrorNotification(actionConfig.getErrorMessage(), actionConfig.getNotification());
        }
    }

    protected boolean doSave() {
        return Objects.isNull(saveFunction) || saveFunction.apply(value);
    }

    protected boolean doCancel() {
        return Objects.isNull(cancelFunction) || cancelFunction.apply(value);
    }

    protected void createInstanceIfNotPresent() {
        if (Objects.isNull(value)) {
            value = createNewInstance();
        }
    }

    protected T createNewInstance() {
        T newInstance = null;
        try {
            newInstance = beanType.getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
                 NoSuchMethodException e) {
            throw new RuntimeException("Bean type must follow the JavaBeans pattern.");
        }
        return newInstance;
    }

    protected void showNotification(NotificationVariant variant, String text, NotificationConfig notificationConfig) {
        Notification notification = new Notification();
        notification.addThemeVariants(variant);
        if (Objects.nonNull(notificationConfig.getDuration())) {
            notification.setDuration((int) notificationConfig.getDuration().toMillis());
        }
        if (Objects.nonNull(notificationConfig.getPosition())) {
            notification.setPosition(notificationConfig.getPosition());
        }

        Div content = new Div(new Text(text));

        Button closeButton = new Button(LumoIcon.CROSS.create());
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
        closeButton.getElement().setAttribute("aria-label", "Close");
        closeButton.addClickListener(event -> notification.close());

        HorizontalLayout layout = new HorizontalLayout(content, closeButton);
        layout.setAlignItems(FlexComponent.Alignment.CENTER);

        notification.add(layout);
        notification.open();
    }

    private void showSuccessNotification(String text, NotificationConfig notificationConfig) {
        showNotification(NotificationVariant.LUMO_SUCCESS, text, notificationConfig);
    }

    private void showErrorNotification(String text, NotificationConfig notificationConfig) {
        showNotification(NotificationVariant.LUMO_ERROR, text, notificationConfig);
    }
}