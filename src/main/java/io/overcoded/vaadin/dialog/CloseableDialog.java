package io.overcoded.vaadin.dialog;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.dialog.DialogVariant;
import com.vaadin.flow.component.dnd.DragSource;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.IconFactory;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.shared.Registration;
import io.overcoded.vaadin.dialog.config.DialogConfig;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static com.vaadin.flow.component.notification.Notification.Position.*;

@Slf4j
public class CloseableDialog extends Dialog implements DragSource<Dialog> {
    private final transient Map<Position, Runnable> positionSetters = Map.of(
            TOP_STRETCH, this::setTopStretchPosition,
            TOP_START, this::setTopStartPosition,
            TOP_CENTER, this::setTopCenterPosition,
            TOP_END, this::setTopEndPosition,
            MIDDLE, this::setMiddlePosition,
            BOTTOM_START, this::setBottomStartPosition,
            BOTTOM_CENTER, this::setBottomCenterPosition,
            BOTTOM_END, this::setBottomEndPosition,
            BOTTOM_STRETCH, this::setBottomStretchPosition
    );
    protected final transient DialogConfig config;

    public CloseableDialog() {
        this(new DialogConfig(), null, null, null);
    }

    public CloseableDialog(DialogConfig config) {
        this(config, null, null, null);
    }

    public CloseableDialog(String title) {
        this(new DialogConfig(), null, title, null);
    }

    public CloseableDialog(IconFactory iconFactory) {
        this(new DialogConfig(), iconFactory, null, null);
    }

    public CloseableDialog(IconFactory iconFactory, String title) {
        this(new DialogConfig(), iconFactory, title, null);
    }

    public CloseableDialog(IconFactory iconFactory, String title, Component content) {
        this(new DialogConfig(), iconFactory, title, content);
    }

    public CloseableDialog(DialogConfig config, IconFactory iconFactory, String title, Component content) {
        this.config = config;

        configure();
        setHeader(iconFactory, title);
        setContent(content);
        registerClientSidePositionChangedListener();
    }

    public Registration addPositionChangedListener(ComponentEventListener<PositionChangedEvent> listener) {
        return addListener(PositionChangedEvent.class, listener);
    }

    public void setPosition(Position position) {
        positionSetters.getOrDefault(position, this::setMiddlePosition).run();
    }

    protected void setTagStyle(String dimension, String value) {
        this.getElement().executeJs("this.$.overlay.$.overlay.style[$0]=$1", dimension, value);
    }

    private void setHeader(IconFactory iconFactory, String text) {
        getHeader().removeAll();
        if (config.getTitle().isEnabled()) {
            HorizontalLayout layout = new HorizontalLayout();
            layout.setAlignItems(FlexComponent.Alignment.CENTER);
            layout.setWidthFull();
            if (Objects.nonNull(iconFactory)) {
                Icon icon = iconFactory.create();
                icon.setSize("var(--lumo-icon-size-m)");
                icon.setColor(config.getTitle().getTextColor());
                layout.add(icon);
            }
            H3 title = new H3(Optional.ofNullable(text).orElse(""));
            title.getElement().getStyle()
                    .set("margin-top", "0")
                    .set("margin-bottom", "0")
                    .set("line-height", "unset")
                    .set("color", config.getTitle().getTextColor());
            layout.add(title);
            layout.setFlexGrow(1, title);
            if (config.getTitle().isCloseable()) {
                Button closeButton = createCloseButton();
                layout.add(closeButton);
            }
            getHeader().add(layout);
        }
    }

    private void configure() {
        if (!config.isPaddingEnabled()) {
            addThemeVariants(DialogVariant.LUMO_NO_PADDING);
        }
        setCloseOnOutsideClick(config.isCloseOnOutsideClick());
        setCloseOnEsc(config.isCloseOnEscape());
        setDraggable(config.isDraggable());
        setResizable(config.isResizable());
        setModal(config.isModal());
        setWidth(config.getWidth());
        setHeight(config.getHeight());
        setMaxWidth(config.getMaxWidth());
        setMaxHeight(config.getMaxHeight());
        if (Objects.nonNull(config.getPosition())) {
            setPosition(config.getPosition());
        }
    }

    public void setContent(Component content) {
        if (Objects.nonNull(content)) {
            content.getStyle()
                    .set("margin-top", "0")
                    .set("padding-top", "0");
            Scroller scroller = new Scroller();
            scroller.setSizeFull();
            removeAll();

            scroller.setContent(content);
            add(scroller);
        }
    }

    private Button createCloseButton() {
        Icon icon = VaadinIcon.CLOSE.create();
        Button closeButton = new Button(icon, e -> this.close());
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY, ButtonVariant.LUMO_CONTRAST, ButtonVariant.LUMO_SMALL);
        closeButton.getStyle().set("margin", "0");
        closeButton.getStyle().set("color", config.getTitle().getTextColor());
        return closeButton;
    }

    /**
     * https://cookbook.vaadin.com/dialog-position-changed-event
     */
    private void registerClientSidePositionChangedListener() {
        // Registers a pseudo position changed listener for the dialog
        // You can extend it with a "mousemove" listener to get position-changed
        // events everytime the mouse is moved around. But beware, this will lead
        // to a massive amount of events fired.
        // You may also extract this JS code into a separate file.
        getElement().executeJs("" +
                "let vaadinOverlay = this.$.overlay;" +
                "let overlay = vaadinOverlay.$.overlay;" +
                "" +
                "overlay.addEventListener('mousedown', e => {" +
                "	let oRect = overlay.getBoundingClientRect();" +
                "	this.__tmp_position_currentX = oRect.x;" +
                "	this.__tmp_position_currentY = oRect.y;" +
                "});" +
                "overlay.addEventListener('mouseup', e => {" +
                "	let oRect = overlay.getBoundingClientRect();" +
                "   " +
                "   if(this.__tmp_position_currentX != oRect.x || this.__tmp_position_currentY != oRect.y) {" +
                "       this.dispatchEvent(new CustomEvent('position-changed', {" +
                "				detail: {" +
                "					x: oRect.x," +
                "					y: oRect.y" +
                "				}" +
                "			}));" +
                "	}" +
                "	delete this.__tmp_position_currentX;" +
                "	delete this.__tmp_position_currentY;" +
                "});");
    }

    private void setTopStretchPosition() {
        String width = getWidth();
        String maxWidth = getMaxWidth();
        setWidth("auto");
        setMaxWidth("100%");
        setTagStyle("top", "0");
        setTagStyle("left", "0");
        setTagStyle("right", "0");
        setTagStyle("position", "absolute");
        if (config.isDraggable()) {
            addPositionChangedListener(event -> {
                setWidth(width);
                setMaxWidth(maxWidth);
                setTagStyle("right", " auto");
            });
        }
    }

    private void setTopStartPosition() {
        setTagStyle("position", "absolute");
        setTagStyle("top", "10px");
        setTagStyle("left", "10px");
    }

    private void setTopCenterPosition() {
        setTagStyle("position", "absolute");
        setTagStyle("top", "10px");
    }

    private void setTopEndPosition() {
        setTagStyle("position", "absolute");
        setTagStyle("top", "10px");
        setTagStyle("right", "10px");
        if (config.isDraggable()) {
            addPositionChangedListener(event -> setTagStyle("right", " auto"));
        }
    }

    private void setMiddlePosition() {
        // do nothing, this is the default
    }

    private void setBottomStartPosition() {
        setTagStyle("position", "absolute");
        setTagStyle("bottom", "10px");
        setTagStyle("left", "10px");
        if (config.isDraggable()) {
            addPositionChangedListener(event -> setTagStyle("bottom", " auto"));
        }
    }

    private void setBottomCenterPosition() {
        setTagStyle("position", "absolute");
        setTagStyle("bottom", "10px");
        if (config.isDraggable()) {
            addPositionChangedListener(event -> setTagStyle("bottom", " auto"));
        }
    }

    private void setBottomEndPosition() {
        setTagStyle("position", "absolute");
        setTagStyle("bottom", "10px");
        setTagStyle("right", "10px");
        if (config.isDraggable()) {
            addPositionChangedListener(event -> {
                setTagStyle("bottom", " auto");
                setTagStyle("right", " auto");
            });
        }
    }

    private void setBottomStretchPosition() {
        String width = getWidth();
        String maxWidth = getMaxWidth();
        setWidth("auto");
        setMaxWidth("100%");
        setTagStyle("left", "0");
        setTagStyle("right", "0");
        setTagStyle("bottom", "0");
        setTagStyle("position", "absolute");
        if (config.isDraggable()) {
            addPositionChangedListener(event -> {
                setWidth(width);
                setMaxWidth(maxWidth);
                setTagStyle("bottom", " auto");
                setTagStyle("right", " auto");
            });
        }
    }
}
