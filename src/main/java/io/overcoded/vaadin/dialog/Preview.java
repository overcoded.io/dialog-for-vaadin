package io.overcoded.vaadin.dialog;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.IconFactory;
import com.vaadin.flow.dom.DomEvent;
import elemental.json.JsonObject;
import io.overcoded.vaadin.dialog.config.DialogConfig;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

@Slf4j
public class Preview extends Div {
    public Preview(String label, String preview) {
        this(PreviewDialogType.DEFAULT.getConfig(), label, preview, null);
    }

    public Preview(DialogConfig config, String label, String preview) {
        this(config, label, preview, null);
    }

    public Preview(String label, String preview, String link) {
        this(PreviewDialogType.DEFAULT.getConfig(), label, preview, link);
    }

    public Preview(DialogConfig config, String label, String preview, String link) {
        IconFactory picture = config.getPreview().getIconFactory();
        PreviewDialog freezeDialog = config.getPreview().isFreezeOnClick() ? createFreezeDialog(preview) : null;
        PreviewDialog dialog = new PreviewDialog(preview);

        Button button = new Button(label, picture.create());
        button.addThemeVariants(config.getPreview().getVariants().toArray(new ButtonVariant[0]));
        button.getElement().setAttribute("aria-label", "Preview");

        configureMouseOver(button, dialog, freezeDialog);
        configureMouseLeaving(button, dialog, freezeDialog);
        if (config.getPreview().isFreezeOnClick()) {
            configureFreezing(button, freezeDialog, dialog);
            configureLinking("dblclick", config, preview, link, button);
        } else {
            configureLinking("click", config, preview, link, button);
        }
        add(button);
    }

    private void configureMouseOver(Button button, PreviewDialog dialog, PreviewDialog freezeDialog) {
        button.getElement()
                .addEventListener("mouseover", event -> open(dialog, freezeDialog, event))
                .addEventData("event.clientX")
                .addEventData("event.clientY");
    }

    private void configureMouseLeaving(Button button, PreviewDialog dialog, PreviewDialog freezeDialog) {
        button.getElement()
                .addEventListener("mouseleave", event -> close(dialog, freezeDialog));
    }

    private void configureFreezing(Button button, PreviewDialog freezeDialog, PreviewDialog dialog) {
        button.getElement()
                .addEventListener("click", event -> freeze(freezeDialog, dialog, event))
                .addEventData("event.clientX")
                .addEventData("event.clientY");
    }

    private void configureLinking(String eventType, DialogConfig config, String preview, String link, Button button) {
        if (config.getPreview().isLinkingAllowed()) {
            String url = Objects.nonNull(link) && !link.isBlank() ? link : preview;
            button.getElement()
                    .addEventListener(eventType, event -> button.getUI().ifPresent(ui -> ui.getPage().open(url, "_blank")));
        }
    }

    private PreviewDialog createFreezeDialog(String preview) {
        PreviewDialog dialog = new PreviewDialog(preview);
        dialog.setCloseOnOutsideClick(true);
        dialog.setCloseOnEsc(true);
        dialog.setDraggable(true);
        return dialog;
    }

    private void close(PreviewDialog dialog, PreviewDialog freezeDialog) {
        if (Objects.isNull(freezeDialog) || !freezeDialog.isOpened()) {
            dialog.close();
        }
    }

    private void freeze(PreviewDialog freezeDialog, PreviewDialog dialog, DomEvent event) {
        dialog.close();
        positioning(freezeDialog, event);
        freeze(freezeDialog);
    }

    private void freeze(PreviewDialog freezeDialog) {
        if (freezeDialog.isOpened()) {
            freezeDialog.close();
        } else {
            freezeDialog.open();
        }
    }

    private void open(PreviewDialog dialog, PreviewDialog freezeDialog, DomEvent event) {
        if (Objects.isNull(freezeDialog) || !freezeDialog.isOpened()) {
            positioning(dialog, event);
            dialog.open();
        }
    }

    private void positioning(PreviewDialog freezeDialog, DomEvent event) {
        JsonObject eventData = event.getEventData();
        double offsetX = eventData.getNumber("event.clientX");
        double offsetY = eventData.getNumber("event.clientY");
        freezeDialog.setPositionTop(offsetY + "px");
        freezeDialog.setPositionLeft(offsetX + "px");
    }
}
