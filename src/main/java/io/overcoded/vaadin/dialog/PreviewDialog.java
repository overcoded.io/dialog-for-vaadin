package io.overcoded.vaadin.dialog;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import io.overcoded.vaadin.dialog.config.DialogConfig;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

@Slf4j
public class PreviewDialog extends CloseableDialog {
    public PreviewDialog(String path) {
        this(PreviewDialogType.DEFAULT.getConfig(), new Image(path, path));
    }

    public PreviewDialog(DialogConfig config, Image image) {
        super(config, null, null, image);
    }

    @Override
    public void setContent(Component content) {
        removeAll();
        if (Objects.nonNull(content)) {
            HorizontalLayout layout = new HorizontalLayout();
            layout.setPadding(false);
            layout.setMargin(false);
            layout.setSpacing(false);
            layout.setWidth(config.getPreview().getWidth());
            layout.setHeight(config.getPreview().getHeight());
            layout.getStyle().set("object-fit", config.getPreview().getObjectFit());
            layout.add(content);
            add(layout);
        }
    }

    protected void setPositionTop(String top) {
        setTagStyle("position", "absolute");
        setTagStyle("top", top);
    }

    protected void setPositionLeft(String left) {
        setTagStyle("position", "absolute");
        setTagStyle("left", left);
    }
}
