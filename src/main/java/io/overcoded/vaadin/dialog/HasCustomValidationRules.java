package io.overcoded.vaadin.dialog;

import com.vaadin.flow.data.binder.Binder;

/**
 * If a FormLayout based form is implementing this interface, the FormDialog will register extra bindings
 * to the FormDialog's Binder.
 * @param <T> type handled by the Form
 */
public interface HasCustomValidationRules<T> {
    void registerBindings(Binder<T> binder);
}
