package io.overcoded.vaadin.dialog;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.EventData;
import lombok.Getter;

@Getter
@DomEvent("position-changed")
public class PositionChangedEvent extends ComponentEvent<CloseableDialog> {
    private final int x;
    private final int y;

    public PositionChangedEvent(CloseableDialog source,
                                boolean fromClient,
                                @EventData("event.detail.x") int x,
                                @EventData("event.detail.y") int y) {
        super(source, fromClient);
        this.x = x;
        this.y = y;
    }
}
