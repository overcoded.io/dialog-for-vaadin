package io.overcoded.vaadin.dialog;

import io.overcoded.vaadin.dialog.config.DialogConfig;
import io.overcoded.vaadin.dialog.config.PreviewConfig;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum PreviewDialogType {
    DEFAULT(DialogType.PREVIEW.getConfig()
            .toBuilder()
            .preview(PreviewConfig.builder().freezeOnClick(true).build())
            .build()),
    WITHOUT_FREEZING(DialogType.PREVIEW.getConfig()
            .toBuilder()
            .preview(PreviewConfig.builder().freezeOnClick(false).build())
            .build()),
    WITHOUT_LINKING(DialogType.PREVIEW.getConfig().toBuilder()
            .preview(PreviewConfig.builder().linkingAllowed(false).build())
            .build()),
    MINIMAL(DialogType.PREVIEW.getConfig().toBuilder()
            .preview(PreviewConfig.builder().freezeOnClick(false).linkingAllowed(false).build())
            .build());

    private final DialogConfig config;
}
