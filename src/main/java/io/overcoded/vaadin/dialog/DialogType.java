package io.overcoded.vaadin.dialog;

import io.overcoded.vaadin.dialog.config.DialogConfig;
import io.overcoded.vaadin.dialog.config.TitleConfig;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum DialogType {
    DEFAULT(DialogConfig.builder().draggable(true).build()),
    PREVIEW(DialogConfig
            .builder()
            .width(null)
            .height(null)
            .modal(false)
            .maxWidth(null)
            .maxHeight(null)
            .closeOnEscape(true)
            .closeOnOutsideClick(true)
            .paddingEnabled(true)
            .title(TitleConfig.builder().enabled(false).closeable(false).build())
            .build());

    private final DialogConfig config;
}
