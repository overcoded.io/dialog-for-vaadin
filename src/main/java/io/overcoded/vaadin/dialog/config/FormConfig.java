package io.overcoded.vaadin.dialog.config;

import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification.Position;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Duration;
import java.util.List;

/**
 * Configuration holder for FormDialog
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class FormConfig {
    /**
     * Enables JSR 303 Bean Validation for the form
     */
    @Builder.Default
    private final boolean beanValidationEnabled = true;
    /**
     * Configuration of the save (or primary) action
     */
    @Builder.Default
    private final FormButtonConfig saveAction = FormButtonConfig.builder()
            .enabled(true)
            .label("Save")
            .closeOnSuccess(true)
            .iconFactory(VaadinIcon.PENCIL)
            .notification(NotificationConfig.builder()
                    .enabled(true)
                    .duration(Duration.ofSeconds(10))
                    .position(Position.TOP_END)
                    .build())
            .variants(List.of(ButtonVariant.LUMO_PRIMARY))
            .successMessage("Dialog content has been saved.")
            .errorMessage("Failed to save dialog. Please contact your system administrator.")
            .build();
    /**
     * Configuration of the secondary action
     */
    @Builder.Default
    private final FormButtonConfig secondaryAction = FormButtonConfig.builder()
            .enabled(true)
            .label("Reset")
            .closeOnSuccess(true)
            .iconFactory(VaadinIcon.REFRESH)
            .notification(NotificationConfig.builder().enabled(false).build())
            .variants(List.of(ButtonVariant.LUMO_ERROR))
            .build();
    /**
     * Configuration of the cancel action
     */
    @Builder.Default
    private final FormButtonConfig cancelAction = FormButtonConfig.builder().enabled(false).build();
}
