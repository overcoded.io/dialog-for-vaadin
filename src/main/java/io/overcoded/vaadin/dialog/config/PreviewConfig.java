package io.overcoded.vaadin.dialog.config;

import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.IconFactory;
import com.vaadin.flow.component.icon.VaadinIcon;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Configuration holder for preview dialogs
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class PreviewConfig {
    /**
     * Enables freezing functionality of the current preview on click.
     * If true, on click, the preview window remained open even if you remove the hover from the label.
     * You can close the frozen dialog by pressing the escape.
     * If false, linking will be available as on click event, if linking is allowed.
     */
    @Builder.Default
    private final boolean freezeOnClick = true;
    /**
     * Enables linking the original image or another url and open it on a new window.
     * If freezeOnClick is enabled, linking action is attached as a double click event, otherwise linking action will
     * be attached as normal click event.
     */
    @Builder.Default
    private final boolean linkingAllowed = true;
    /**
     * Width of the preview layout
     */
    @Builder.Default
    private final String width = "100%";
    /**
     * Height of the preview layout
     */
    @Builder.Default
    private final String height = "250px";
    /**
     * Sets how the content of the preview image should be resized to fit its container
     */
    @Builder.Default
    private final String objectFit = "cover";
    /**
     * Icon which should be displayed before the element to mark the preview
     */
    @Builder.Default
    private final IconFactory iconFactory = VaadinIcon.PICTURE;
    /**
     * Button variants of the holder button, which is used by the preview
     */
    @Builder.Default
    private final List<ButtonVariant> variants = List.of(ButtonVariant.LUMO_ICON, ButtonVariant.LUMO_TERTIARY, ButtonVariant.LUMO_CONTRAST);
}
