package io.overcoded.vaadin.dialog.config;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.IconFactory;
import com.vaadin.flow.component.shared.ThemeVariant;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * Configuration holder of buttons on form
 */
@Data
@Builder(toBuilder = true)
public class FormButtonConfig {
    /**
     * @see com.vaadin.flow.component.button.Button#setText(String)
     */
    private final String label;
    /**
     * Defines that the button should be added to the footer or not
     */
    private final boolean enabled;
    /**
     * Error message which should appear as a notification in case of issues with the action triggered by this button
     */
    private final String errorMessage;
    /**
     * Success message which should appear as a notification in case of issues with the action, triggered by this button
     */
    private final String successMessage;
    /**
     * If true, the dialog will be closed automatically, when the action attached to the button succeeded
     */
    private final boolean closeOnSuccess;
    /**
     * Defines the icon of the button
     * @see com.vaadin.flow.component.button.Button#setIcon(Component)
     */
    private final IconFactory iconFactory;
    /**
     * Defines the Button variants for the button
     * @see com.vaadin.flow.component.button.Button#addThemeVariants(ThemeVariant[]) 
     */
    private final List<ButtonVariant> variants;
    /**
     * Configuration of the notification which appears in case of success or error
     */
    private final NotificationConfig notification;
}
