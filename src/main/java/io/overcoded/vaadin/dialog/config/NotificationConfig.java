package io.overcoded.vaadin.dialog.config;

import com.vaadin.flow.component.notification.Notification.Position;
import lombok.Builder;
import lombok.Data;

import java.time.Duration;

/**
 * Configuration holder of notification
 */
@Data
@Builder(toBuilder = true)
public class NotificationConfig {
    /**
     * Is the notification enabled or not. If false, success notification will be disabled and won't appear.
     * Error notification will be displayed, even if this configuration is set to false.
     */
    private final boolean enabled;
    /**
     * Where should the notification appear
     *
     * @see com.vaadin.flow.component.notification.Notification#setPosition(Position)
     */
    private final Position position;
    /**
     * How long should the notification appear. If not defined, it will follow the notification default configuration,
     * and notifications will stay on-screen for five seconds.
     *
     * @see com.vaadin.flow.component.notification.Notification#setDuration(int)
     */
    private final Duration duration;
}
