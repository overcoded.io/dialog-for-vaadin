package io.overcoded.vaadin.dialog.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Configuration holder for the CloseableDialog's title
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class TitleConfig {
    /**
     * If false, title won't be visible, not even the close button
     */
    @Builder.Default
    private final boolean enabled = true;
    /**
     * If true, adds the close button to the header
     */
    @Builder.Default
    private final boolean closeable = true;
    /**
     * Color of title (icon, label and close button) in the header
     */
    @Builder.Default
    private final String textColor = "var(--lumo-contrast-70pct)";
}
