package io.overcoded.vaadin.dialog.config;

import com.vaadin.flow.component.notification.Notification.Position;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Configuration holder for ClosableDialog and its descendant (FormDialog and PreviewDialog).
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class DialogConfig {
    /**
     * The position of a ClosableDialog.
     * @see Position
     */
    @Builder.Default
    private final Position position = Position.MIDDLE;
    /**
     * Title related configurations
     */
    @Builder.Default
    private final TitleConfig title = TitleConfig.builder().build();
    /**
     * Configuration of Preview dialog specific configuration
     */
    @Builder.Default
    private final PreviewConfig preview = PreviewConfig.builder().build();
    /**
     * Configurations of FormDialog dialog specific configuration.
     */
    @Builder.Default
    private final FormConfig form = FormConfig.builder().build();
    /**
     * @see com.vaadin.flow.component.dialog.DialogVariant#LUMO_NO_PADDING
     */
    @Builder.Default
    private final boolean paddingEnabled = false;
    /**
     * @see com.vaadin.flow.component.dialog.Dialog#setCloseOnOutsideClick(boolean)
     */
    @Builder.Default
    private final boolean closeOnOutsideClick = false;
    /**
     * @see com.vaadin.flow.component.dialog.Dialog#setCloseOnEsc(boolean)
     */
    @Builder.Default
    private final boolean closeOnEscape = false;
    /**
     * @see com.vaadin.flow.component.dialog.Dialog#setDraggable(boolean)
     */
    @Builder.Default
    private final boolean draggable = false;
    /**
     * @see com.vaadin.flow.component.dialog.Dialog#setResizable(boolean)
     */
    @Builder.Default
    private final boolean resizable = false;
    /**
     * @see com.vaadin.flow.component.dialog.Dialog#setModal(boolean)
     */
    @Builder.Default
    private final boolean modal = true;
    /**
     * @see com.vaadin.flow.component.dialog.Dialog#setWidth(String)
     */
    @Builder.Default
    private final String width = "auto";
    /**
     * @see com.vaadin.flow.component.dialog.Dialog#setHeight(String)
     */
    @Builder.Default
    private final String height = "auto";
    /**
     * @see com.vaadin.flow.component.dialog.Dialog#setMaxWidth(String)
     */
    @Builder.Default
    private final String maxWidth = "500px";
    /**
     * @see com.vaadin.flow.component.dialog.Dialog#setMaxHeight(String)
     */
    @Builder.Default
    private final String maxHeight = "auto";
}
