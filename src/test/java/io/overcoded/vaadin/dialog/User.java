package io.overcoded.vaadin.dialog;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.*;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "username")
public class User {
    private String firstName;

    private String lastName;

    @NotEmpty
    @Pattern(regexp = "^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$", message = "Username must not contain special characters")
    private String username;

    @NotEmpty
    @Size(min = 7, max = 200)
    private String password;
}
