package io.overcoded.vaadin.dialog;

import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ErrorLevel;
import com.vaadin.flow.data.binder.ValidationResult;
import com.vaadin.flow.data.binder.Validator;

import java.util.Objects;


public class UserForm extends FormLayout implements HasCustomValidationRules<User> {
    private final TextField firstName = new TextField("First name");
    private final TextField lastName = new TextField("Last name");
    private final TextField username = new TextField("Username");
    private final PasswordField password = new PasswordField("Password");
    private final PasswordField confirmPassword = new PasswordField("Confirm password");

    public UserForm() {
        add(firstName, lastName, username, password, confirmPassword);
        setResponsiveSteps(new ResponsiveStep("0", 1), new ResponsiveStep("250px", 2));
        setColspan(username, 2);
    }

    @Override
    public void registerBindings(Binder<User> binder) {
        binder.forField(confirmPassword)
                .withValidator((Validator<String>) (value, valueContext) -> Objects.equals(password.getValue(), value)
                        ? ValidationResult.ok()
                        : ValidationResult.create("Passwords do not match.", ErrorLevel.ERROR))
                .bind("password");
    }
}
