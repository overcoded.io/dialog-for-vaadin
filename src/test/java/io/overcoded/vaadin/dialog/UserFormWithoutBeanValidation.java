package io.overcoded.vaadin.dialog;

import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ErrorLevel;
import com.vaadin.flow.data.binder.ValidationResult;

import java.util.Objects;
import java.util.regex.Pattern;


public class UserFormWithoutBeanValidation extends FormLayout implements HasCustomValidationRules<User> {
    private final Pattern pattern = Pattern.compile("^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$");
    private final TextField firstName = new TextField("First name");
    private final TextField lastName = new TextField("Last name");
    private final TextField username = new TextField("Username");
    private final PasswordField password = new PasswordField("Password");
    private final PasswordField confirmPassword = new PasswordField("Confirm password");

    public UserFormWithoutBeanValidation() {
        add(firstName, lastName, username, password, confirmPassword);
        setResponsiveSteps(new ResponsiveStep("0", 1), new ResponsiveStep("250px", 2));
        setColspan(username, 2);
    }

    @Override
    public void registerBindings(Binder<User> binder) {
        binder.forField(username)
                .asRequired()
                .withValidator((value, valueContext) -> pattern.matcher(value).find()
                        ? ValidationResult.ok()
                        : ValidationResult.create("Username should not contain any special characters", ErrorLevel.ERROR))
                .bind("username");
        binder.forField(password)
                .asRequired()
                .withValidator((value, valueContext) -> value.length() > 6
                        ? ValidationResult.ok()
                        : ValidationResult.create("Password must be longer than 6 characters", ErrorLevel.ERROR))
                .bind("password");
        binder.forField(confirmPassword)
                .withValidator((value, valueContext) -> Objects.equals(password.getValue(), value)
                        ? ValidationResult.ok()
                        : ValidationResult.create("Passwords do not match.", ErrorLevel.ERROR))
                .bind("password");
    }
}
