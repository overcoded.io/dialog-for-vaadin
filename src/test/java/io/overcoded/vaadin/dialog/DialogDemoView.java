package io.overcoded.vaadin.dialog;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.IconFactory;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;
import java.util.function.Consumer;

@Slf4j
@RouteAlias(value = "/", layout = MainLayout.class)
@Route(value = "/dialog", layout = MainLayout.class)
public class DialogDemoView extends VerticalLayout {

    public DialogDemoView() {
        setWidthFull();

        H1 title = new H1("Closable dialog for Vaadin");
        title.getStyle().set("font-size", "var(--lumo-font-size-l)").set("margin", "0");
        add(title);
        Html introduction = new Html("""
                <section>
                <p>
                    Closable dialogs are dialogs with a default close button and title. <code>ClosableDialog</code> comes with a configuration class, where mostly regular things can be configured,
                    like close on escape, etc. Which also can be configured with default methods. It is also supporting the title configuration.
                </p>
                </section
                """);
        add(introduction);

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setAlignItems(Alignment.START);
        horizontalLayout.add(createDemoButton("With icon and text", VaadinIcon.EDIT, "TinyMCE"));
        horizontalLayout.add(createDemoButton("Without icon", null, "TinyMCE"));
        horizontalLayout.add(createDemoButton("Without text", VaadinIcon.EDIT, null));
        horizontalLayout.add(createDemoButton("Without icon and text", null, null));
        add(horizontalLayout);

        title = new H1("Positionable dialog for Vaadin");
        title.getStyle().set("font-size", "var(--lumo-font-size-l)").set("margin", "0");
        add(title);
        Html positionIntroduction = new Html("""
                <section>
                <p>
                    Closable dialog also supports the same positions just like <a href="https://vaadin.com/docs/latest/components/notification#position" target="_blank">Notification</a>.
                </p>
                </section
                """);
        add(positionIntroduction);

        add(createGrid());
    }

    private Button createDemoButton(String label, IconFactory iconFactory, String title) {
        Button button = new Button(label);
        button.addClickListener(event -> {
            CloseableDialog dialog = new CloseableDialog(DialogType.DEFAULT.getConfig(), iconFactory, title, createContent());
            dialog.open();
        });
        return button;
    }

    private VerticalLayout createContent() {
        VerticalLayout layout = new VerticalLayout();
        layout.add(new Html("""
                <div>
                    <p>Over <strong>1.5M developers agree</strong> TinyMCE is the rich text editor of choice. With an open-source core and additional premium add-ons, TinyMCE scales with your app as you grow.</p>
                    <p>Use TinyMCE as:</p>
                    <ul>
                        <li>A <strong>basic</strong> editor</li>
                        <li>An advanced 📝 editor</li>
                        <li>A {{template-based}} editor</li>
                        <li>A totally <code>&lt;customized&gt;</code> editor</li>
                    </ul>
                    <p><strong>Try out this demo to see how it works!</strong></p>
                    <p>
                        <img style="max-width: 100%;" role="presentation" src="blob:https://www.tiny.cloud/97fa9c6e-2072-4e22-9ade-8784fe3a8fea" alt="" width="316">
                    </p>
                    <p style="text-align: right;"><a href="https://www.tiny.cloud/" target="_blank">https://www.tiny.cloud/</a></p>
                </div>
                """));
        return layout;
    }

    private VerticalLayout createLipsumContent() {
        Paragraph paragraph = new Paragraph("Vivamus eu mi nibh. Etiam dictum ipsum nunc, a imperdiet sem consectetur eget. Cras dictum turpis nibh, vitae sollicitudin lacus posuere sed. Nulla viverra porttitor quam, ac facilisis diam varius non. Etiam posuere facilisis blandit. Suspendisse fringilla, nunc ut mollis rutrum, lacus velit porttitor nisi, eget placerat ante elit ut metus. Cras ut tortor lacus. Mauris id aliquam ex. Quisque id elit vestibulum neque maximus blandit. Fusce odio velit, auctor sit amet nibh vitae, imperdiet ornare nulla.");
        VerticalLayout layout = new VerticalLayout(paragraph);
        layout.setWidthFull();
        return layout;
    }

    private Div createGrid() {
        Div div = new Div();
        div.setWidthFull();
        div.setClassName("position-example");
        div.add(createButton(Position.TOP_STRETCH),
                createButton(Position.TOP_START),
                createButton(Position.TOP_CENTER),
                createButton(Position.TOP_END),
                createButton(Position.MIDDLE),
                createButton(Position.BOTTOM_START),
                createButton(Position.BOTTOM_CENTER),
                createButton(Position.BOTTOM_END),
                createButton(Position.BOTTOM_STRETCH)
        );
        return div;
    }

    private Button createButton(Position position) {
        Button button = new Button(position.getClientName());
        button.addClickListener(event -> show(position));
        return button;
    }

    private void show(Position position) {
        CloseableDialog dialog = new CloseableDialog(VaadinIcon.WARNING, "Lorem Ipsum", createLipsumContent());
        dialog.setPosition(position);
        dialog.setDraggable(true);
        dialog.open();
    }
}
