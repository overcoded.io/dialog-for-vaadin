package io.overcoded.vaadin.dialog;

import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.textfield.IntegerField;

public class ScaleForm extends FormLayout {
    private final IntegerField numberOfInstances = new IntegerField();

    public ScaleForm() {
        numberOfInstances.setMin(0);
        numberOfInstances.setMax(1000);
        numberOfInstances.setStep(1);
        numberOfInstances.setStepButtonsVisible(true);
        setResponsiveSteps(
                new ResponsiveStep("50", 1),
                new ResponsiveStep("100", 1),
                new ResponsiveStep("50", 1)
        );
        add(new Div());
        add(numberOfInstances);
        add(new Div());
    }
}
