package io.overcoded.vaadin.dialog;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.html.Pre;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import io.overcoded.vaadin.dialog.config.DialogConfig;
import io.overcoded.vaadin.dialog.config.FormButtonConfig;
import io.overcoded.vaadin.dialog.config.FormConfig;
import io.overcoded.vaadin.dialog.config.NotificationConfig;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.util.List;

@Slf4j
@Route(value = "/form", layout = MainLayout.class)
public class FormDemoView extends VerticalLayout {
    public FormDemoView() {
        setWidthFull();

        H1 title = new H1("Form dialog for Vaadin");
        title.getStyle().set("font-size", "var(--lumo-font-size-l)").set("margin", "0");
        add(title);
        Html introduction = new Html("""
                <section>
                <p>
                    Form dialogs are dialogs which should hold a <code>FormLayout</code> which is responsible to edit a
                    bean type. Form dialogs are using <a href="https://vaadin.com/docs/latest/binding-data/components-binder-beans#automatic-data-binding" target="_blank">automatic data biding</a>
                    so the best way to use <code>FormDialog</code> with a bean type (e.g.: <code>User</code>) to create a form (e.g.: <code>UserForm</code>) extending the <code>FormLayout</code> for it.<br>
                    By default <code>FormDialog</code> also <a href="https://vaadin.com/docs/latest/binding-data/components-binder-beans#using-jsr-303-bean-validation" target="_blank">using JSR 303 Bean Validation</a>, but this can be turn off through the dialogs configuration class.
                </p>
                </section
                """);
        add(introduction);


        Accordion content = new Accordion();
        content.add("With bean validation (default)", getFormDialogWithBeanValidation());
        content.add("Without bean validation", getFormDialogWithoutBeanValidation());
        content.add("Compact form", getCompactFormDialog());
        content.add("Custom form", getCustomFormDialog());
        content.setWidthFull();

        add(content);
    }

    private VerticalLayout getFormDialogWithBeanValidation() {
        VerticalLayout layout = new VerticalLayout();
        Paragraph introduction = new Paragraph("The default configuration comes with bean validation support, buttons with icon and notification feedbacks.");
        FormDialog<User> dialog = new FormDialog<>(User.class, VaadinIcon.USER, "Create a new user", new UserForm());
        Button demo = new Button("Open dialog", event -> dialog.open());
        Pre code = new Pre("""
                FormDialog<User> dialog = new FormDialog<>(User.class, VaadinIcon.USER, "Create a new user", new UserForm());
                Button demo = new Button("Open dialog", event -> dialog.open());
                """);
        code.setWidthFull();
        layout.add(introduction, demo, code);
        layout.setAlignSelf(Alignment.CENTER, demo);
        layout.setWidthFull();
        return layout;
    }

    private VerticalLayout getFormDialogWithoutBeanValidation() {
        VerticalLayout layout = new VerticalLayout();
        Paragraph introduction = new Paragraph("There is a built in configuration without bean validation if you want to avoid to add any JSR 303 implementation.");
        DialogConfig config = FormDialogType.DEFAULT_WITHOUT_BEAN_VALIDATION.getConfig();
        FormDialog<User> dialog = new FormDialog<>(User.class, config, VaadinIcon.USER, "Update user", new UserFormWithoutBeanValidation());
        dialog.setValue(User.builder().firstName("John").lastName("Smith").username("john.smith").password("********").build());
        Button demo = new Button("Open dialog", event -> {
            dialog.setSaveFunction(modifiedUser -> {
                log.info("You can do anything with this {}. If this method returns with false, the dialog won't close.", modifiedUser);
                return true;
            });
            dialog.open();
        });
        Pre code = new Pre("""
                DialogConfig config = FormDialogType.DEFAULT_WITHOUT_BEAN_VALIDATION.getConfig();
                FormDialog<User> dialog = new FormDialog<>(User.class, config, VaadinIcon.USER, "Update user", new UserFormWithoutBeanValidation());
                dialog.setValue(User.builder().firstName("John").lastName("Smith").username("john.smith").password("********").build());
                Button demo = new Button("Open dialog", event -> {
                    dialog.setSaveFunction(modifiedUser -> {
                        log.info("You can do anything with this {}. If this method returns with false, the dialog won't close.", modifiedUser);
                        return true;
                    });
                    dialog.open();
                });
                """);
        code.setWidthFull();
        layout.add(introduction, demo, code);
        layout.setAlignSelf(Alignment.CENTER, demo);
        layout.setWidthFull();
        return layout;
    }

    private VerticalLayout getCompactFormDialog() {
        VerticalLayout layout = new VerticalLayout();
        Paragraph introduction = new Paragraph("There is a configuration for displaying a compact form, which removes the header and puts an extra button to close the dialog. This configuration also have a version without bean validation.");
        DialogConfig config = FormDialogType.COMPACT.getConfig();
        FormDialog<User> dialog = new FormDialog<>(User.class, config, VaadinIcon.USER, "Update user", new UserForm());
        dialog.setValue(User.builder().firstName("John").lastName("Smith").username("john.smith").password("********").build());
        Button demo = new Button("Open dialog", event -> {
            dialog.setSaveFunction(modifiedUser -> {
                log.info("You can do anything with this {}. If this method returns with false, the dialog won't close.", modifiedUser);
                return true;
            });
            dialog.open();
        });
        Pre code = new Pre("""
                DialogConfig config = FormDialogType.COMPACT.getConfig();
                FormDialog<User> dialog = new FormDialog<>(User.class, config, VaadinIcon.USER, "Update user", new UserFormWithoutBeanValidation());
                dialog.setValue(User.builder().firstName("John").lastName("Smith").username("john.smith").password("********").build());
                Button demo = new Button("Open dialog", event -> {
                    dialog.open();
                });
                """);
        code.setWidthFull();
        layout.add(introduction, demo, code);
        layout.setAlignSelf(Alignment.CENTER, demo);
        layout.setWidthFull();
        return layout;
    }

    private VerticalLayout getCustomFormDialog() {
        DialogConfig config = DialogConfig.builder()
                .form(FormConfig.builder()
                        .beanValidationEnabled(false)
                        .saveAction(FormButtonConfig.builder()
                                .enabled(true)
                                .label("Scale")
                                .closeOnSuccess(true)
                                .notification(NotificationConfig.builder()
                                        .enabled(true)
                                        .duration(Duration.ofSeconds(10))
                                        .position(Notification.Position.TOP_END)
                                        .build())
                                .iconFactory(VaadinIcon.SAFE_LOCK)
                                .variants(List.of(ButtonVariant.LUMO_PRIMARY))
                                .successMessage("Deployment has been scaled.")
                                .errorMessage("Failed to scale service. Please contact your system administrator.")
                                .build())
                        .cancelAction(FormButtonConfig.builder().enabled(false).build())
                        .secondaryAction(FormButtonConfig.builder()
                                .enabled(true)
                                .label("Reset")
                                .closeOnSuccess(false)
                                .notification(NotificationConfig.builder().enabled(false).build())
                                .iconFactory(VaadinIcon.REFRESH)
                                .variants(List.of(ButtonVariant.LUMO_ERROR))
                                .build())
                        .build())
                .build();

        Paragraph introduction = new Paragraph("Beside the predefined configuration, you can define your own configuration to have custom buttons and actions.");

        FormDialog<Scale> dialog = new FormDialog<>(Scale.class, config, VaadinIcon.CUBES, "Scale deployment", new ScaleForm());
        dialog.setValue(Scale.builder().numberOfInstances(5).build());
        dialog.setSaveFunction(scale -> {
            log.info("Any custom logic can happen here, with the new value: {}", scale);
            return true;
        });

        Button demo = new Button("Open dialog", event -> dialog.open());

        Pre code = new Pre("""
                FormDialog<Scale> dialog = new FormDialog<>(Scale.class, config, VaadinIcon.CUBES, "Scale deployment", new ScaleForm());
                dialog.setValue(Scale.builder().numberOfInstances(5).build());
                dialog.setSaveFunction(scale -> {
                    log.info("Any custom logic can happen here, with the new value: {}", scale);
                    return true;
                });
                                
                Button demo = new Button("Open dialog", event -> dialog.open());
                Button demo = new Button("Open dialog", event -> {
                    dialog.open();
                });
                """);
        code.setWidthFull();

        Paragraph note = new Paragraph("The configuration comes with a builder, you can override everything or you can keep the default values. Here is a complete example of configuration:");

        Pre configCode = new Pre("""
                DialogConfig config = DialogConfig.builder()
                    .form(FormConfig.builder()
                            .beanValidationEnabled(false)
                            .saveAction(FormButtonConfig.builder()
                                    .enabled(true)
                                    .label("Scale")
                                    .closeOnSuccess(true)
                                    .notification(NotificationConfig.builder()
                                            .enabled(true)
                                            .duration(Duration.ofSeconds(10))
                                            .position(Notification.Position.TOP_END)
                                            .build())
                                    .iconFactory(VaadinIcon.SAFE_LOCK)
                                    .variants(List.of(ButtonVariant.LUMO_PRIMARY))
                                    .successMessage("Deployment has been scaled.")
                                    .errorMessage("Failed to scale service. Please contact your system administrator.")
                                    .build())
                            .cancelAction(FormButtonConfig.builder().enabled(false).build())
                            .secondaryAction(FormButtonConfig.builder()
                                    .enabled(true)
                                    .label("Reset")
                                    .closeOnSuccess(false)
                                    .notification(NotificationConfig.builder().enabled(false).build())
                                    .iconFactory(VaadinIcon.REFRESH)
                                    .variants(List.of(ButtonVariant.LUMO_ERROR))
                                    .build())
                            .build())
                    .build();
                """);
        configCode.setWidthFull();

        VerticalLayout layout = new VerticalLayout();
        layout.add(introduction, demo, code, note, configCode);
        layout.setAlignSelf(Alignment.CENTER, demo);
        layout.setWidthFull();

        return layout;
    }
}
