package io.overcoded.vaadin.dialog;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import io.overcoded.vaadin.dialog.config.DialogConfig;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@CssImport("./position-example.css")
@Route(value = "/preview", layout = MainLayout.class)
public class PreviewDemoView extends VerticalLayout {
    public PreviewDemoView() {
        setWidthFull();

        H1 title = new H1("Preview dialog for Vaadin");
        title.getStyle().set("font-size", "var(--lumo-font-size-l)").set("margin", "0");
        add(title);
        Html introduction = new Html("""
                <section>
                <p>
                    <code>Preview</code> is a dialog which can be displayed on hover action to create preview of an image.
                    Once the hover left the preview link, the preview dialog will be closed. <code>Preview</code> also
                    supporting linking and freezing features. Freezing is keeping the preview dialog open, the linking
                    opens the original image on a new window.<br>
                    If freezing is enabled, linking can be used with double click. If freezing is disabled, linking available
                    with normal click.
                </p>
                </section
                """);
        add(introduction);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(new Preview("With freezing and linking", "https://static.liligo.com/img/cms/location-gallery/hungary/budapest/dan-freeman-407984-unsplash/thumbnail.webp", "https://static.liligo.com/img/cms/location-gallery/hungary/budapest/dan-freeman-407984-unsplash/image.webp"));
        DialogConfig config = PreviewDialogType.WITHOUT_FREEZING.getConfig();
        verticalLayout.add(new Preview(config, "Without freezing", "https://static.liligo.com/img/cms/location-gallery/hungary/budapest/dan-freeman-407984-unsplash/thumbnail.webp", "https://static.liligo.com/img/cms/location-gallery/hungary/budapest/dan-freeman-407984-unsplash/image.webp"));
        verticalLayout.add(new Preview(PreviewDialogType.WITHOUT_LINKING.getConfig(), "Without linking", "https://static.liligo.com/img/cms/location-gallery/france/paris/thomas-kelley-75110-unsplash/thumbnail.webp"));
        verticalLayout.add(new Preview(PreviewDialogType.MINIMAL.getConfig(), "Without linking and freezing", "https://static.liligo.com/img/cms/location-gallery/spain/barcelona/david-sola-522934-unsplash/thumbnail.webp"));
        verticalLayout.add(new Preview("Default (with large image)", "https://static.liligo.com/img/cms/location-gallery/hungary/budapest/dan-freeman-407984-unsplash/image.webp"));
        add(verticalLayout);
    }
}
