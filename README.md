# dialog-for-vaadin
Extending Vaadin's dialog to simplify creation of closable, preview or form dialogs. 

## Closable dialog
Closable dialogs are dialogs with a default close button and title. `ClosableDialog` comes with a configuration class, 
where mostly regular things can be configured, like close on escape, etc. Which also can be configured with default methods. 
It is also supporting the title configuration.

![closable-dialog-default.png](screenshots/closable-dialog-default.png)

```java
CloseableDialog dialog = new CloseableDialog(VaadinIcon.WARNING, "Warning", createWarningContent());
```

or 

```java
DialogConfig config = DialogConfig.builder().title(TitleConfig.builder().enabled(false).build()).build();
CloseableDialog dialog = new CloseableDialog(config);
```

Closable dialogs comes with a configuration class, where you can configure the built-in properties of a `Dialog`, like:

- modal
- closeable
- draggable
- resizable
- close on outside click
- close on escape
- width (+max width)
- height (+max height)

Through the configuration, title can be enabled or disabled, and also the color of the title text can be defined.

#### Positioning
Closable dialog comes with positioning support based on [Notification](https://vaadin.com/docs/latest/components/notification#position)'s positions.
This can be configured through the configuration class, or simple setting the position before opening the dialog.

```java
CloseableDialog dialog = new CloseableDialog(VaadinIcon.WARNING, "Lorem Ipsum", createLipsumContent());
dialog.setPosition(Position.BOTTOM_END);
dialog.setDraggable(true);
dialog.open();
```

![closable-dialog-bottom-end.png](screenshots/Fclosable-dialog-bottom-end.png)

## Form dialog
Form dialogs are dialogs which should hold a <code>FormLayout</code> which is responsible to edit a
bean type. Form dialogs are using <a href="https://vaadin.com/docs/latest/binding-data/components-binder-beans#automatic-data-binding" target="_blank">automatic data biding</a>
so the best way to use <code>FormDialog</code> with a bean type (e.g.: <code>User</code>) to create a form (e.g.: <code>UserForm</code>) extending the <code>FormLayout</code> for it.<br>
By default <code>FormDialog</code> also <a href="https://vaadin.com/docs/latest/binding-data/components-binder-beans#using-jsr-303-bean-validation" target="_blank">using JSR 303 Bean Validation</a>, but this can be turn off through the dialogs configuration class.

![form-dialog-default.png](screenshots/form-dialog-default.png)

### With bean validation (default)
The default configuration comes with bean validation support, buttons with icon and notification feedbacks.

```java
FormDialog<User> dialog = new FormDialog<>(User.class, VaadinIcon.USER, "Create a new user", new UserForm());
Button demo = new Button("Open dialog", event -> dialog.open());
```

### Without bean validation
There is a built in configuration without bean validation if you want to avoid to add any JSR 303 implementation.

```java
DialogConfig config = FormDialogType.DEFAULT_WITHOUT_BEAN_VALIDATION.getConfig();
FormDialog<User> dialog = new FormDialog<>(User.class, config, VaadinIcon.USER, "Update user", new UserFormWithoutBeanValidation());
dialog.setValue(User.builder().firstName("John").lastName("Smith").username("john.smith").password("********").build());
Button demo = new Button("Open dialog", event -> {
    dialog.setSaveFunction(modifiedUser -> {
        log.info("You can do anything with this {}. If this method returns with false, the dialog won't close.", modifiedUser);
        return true;
    });
    dialog.open();
});
```

### Compact form
There is a configuration for displaying a compact form, which removes the header and puts an extra button to close the dialog. This configuration also have a version without bean validation.

```java
DialogConfig config = FormDialogType.COMPACT.getConfig();
FormDialog<User> dialog = new FormDialog<>(User.class, config, VaadinIcon.USER, "Update user", new UserFormWithoutBeanValidation());
dialog.setValue(User.builder().firstName("John").lastName("Smith").username("john.smith").password("********").build());
Button demo = new Button("Open dialog", event -> {
    dialog.open();
});
```

![form-dialog-compact.png](screenshots/form-dialog-compact.png)

### Custom form
Beside the predefined configuration, you can define your own configuration to have custom buttons and actions.

```java
FormDialog<Scale> dialog = new FormDialog<>(Scale.class, config, VaadinIcon.CUBES, "Scale deployment", new ScaleForm());
dialog.setValue(Scale.builder().numberOfInstances(5).build());
dialog.setSaveFunction(scale -> {
    log.info("Any custom logic can happen here, with the new value: {}", scale);
    return true;
});
                
Button demo = new Button("Open dialog", event -> dialog.open());
Button demo = new Button("Open dialog", event -> {
    dialog.open();
});
```

![form-dialog-custom.png](screenshots/form-dialog-custom.png)

The configuration comes with a builder, you can override everything or you can keep the default values. Here is a complete example of configuration:

```java
DialogConfig config = DialogConfig.builder()
    .form(FormConfig.builder()
            .beanValidationEnabled(false)
            .saveAction(FormButtonConfig.builder()
                    .enabled(true)
                    .label("Scale")
                    .closeOnSuccess(true)
                    .notification(NotificationConfig.builder()
                            .enabled(true)
                            .duration(Duration.ofSeconds(10))
                            .position(Notification.Position.TOP_END)
                            .build())
                    .iconFactory(VaadinIcon.SAFE_LOCK)
                    .variants(List.of(ButtonVariant.LUMO_PRIMARY))
                    .successMessage("Deployment has been scaled.")
                    .errorMessage("Failed to scale service. Please contact your system administrator.")
                    .build())
            .cancelAction(FormButtonConfig.builder().enabled(false).build())
            .secondaryAction(FormButtonConfig.builder()
                    .enabled(true)
                    .label("Reset")
                    .closeOnSuccess(false)
                    .notification(NotificationConfig.builder().enabled(false).build())
                    .iconFactory(VaadinIcon.REFRESH)
                    .variants(List.of(ButtonVariant.LUMO_ERROR))
                    .build())
            .build())
    .build();
```

## Preview dialog
`Preview` is a dialog which can be displayed on hover action to create preview of an image. Once the hover left the 
preview link, the preview dialog will be closed. `Preview` also supporting linking and freezing features. 
You only need to add these `Preview` instances to your layout.

![preview-default.png](screenshots/preview-default.png)

## With freezing and linking (default)
If freezing is enabled, one click on the preview label will freeze the preview dialog. This dialog is draggable, so you
can move it around. Close on escape also supported, so you can close frozen dialogs.  
Linking, opening an url on a new window, also working, but in case of freezing is enabled, you need to use double click. 

```java
Preview preview = new Preview("With freezing and linking", "thumbnail.webp", "image.webp")
```

![preview-with-freezing.png](screenshots%2Fpreview-with-freezing.png)


## Without freezing
Freezing can be disabled. In this case, linking is working with a single click. 

```java
DialogConfig config = PreviewDialogType.WITHOUT_FREEZING.getConfig();
Preview preview = new Preview(config, "Without freezing", "image.webp");
```

## Without linking
Linking also can be disabled through the configuration class. 

```java
Preview preview = new Preview(PreviewDialogType.WITHOUT_LINKING.getConfig(), "Without linking", "thumbnail.webp");
```

## Without linking and freezing 
there is also a buil- in configuration to disable both functionality and provide simple preview without any extra functionality. 

```java
Preview preview = new Preview(PreviewDialogType.MINIMAL.getConfig(), "Without linking and freezing", "thumbnail.webp")
```
