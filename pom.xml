<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>io.overcoded</groupId>
    <artifactId>dialog-for-vaadin</artifactId>
    <version>24.1.1-SNAPSHOT</version>
    <name>dialog-for-vaadin</name>
    <url>https://gitlab.com/overcoded.io/dialog-for-vaadin</url>
    <description>Configurable dialog for Vaadin</description>

    <developers>
        <developer>
            <id>belakede</id>
            <name>Adam Belak</name>
            <timezone>Europe/Budapest</timezone>
        </developer>
    </developers>

    <licenses>
        <license>
            <name>Apache License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <scm>
        <url>https://gitlab.com/overcoded.io/dialog-for-vaadin</url>
        <connection>scm:git:git@gitlab.com:overcoded.io/dialog-for-vaadin.git</connection>
        <developerConnection>scm:git:git@gitlab.com:overcoded.io/dialog-for-vaadin.git</developerConnection>
        <tag>HEAD</tag>
    </scm>

    <issueManagement>
        <system>GitLab Issue Tracker</system>
        <url>https://gitlab.com/overcoded.io/dialog-for-vaadin/-/issues</url>
    </issueManagement>

    <properties>
        <projectId />
        <java.version>17</java.version>
        <file.encoding>UTF-8</file.encoding>
        <maven.compiler.source>${java.version}</maven.compiler.source>
        <maven.compiler.target>${java.version}</maven.compiler.target>
        <project.build.sourceEncoding>${file.encoding}</project.build.sourceEncoding>
        <project.reporting.outputEncoding>${file.encoding}</project.reporting.outputEncoding>

        <vaadin.version>24.1.4</vaadin.version>
        <lombok.version>1.18.28</lombok.version>
        <jetty.version>11.0.15</jetty.version>
        <flow-server-production-mode.version>24.1.4</flow-server-production-mode.version>

        <jacoco-maven-plugin.version>0.8.7</jacoco-maven-plugin.version>
        <maven-failsafe-plugin.version>3.1.2</maven-failsafe-plugin.version>
        <maven-surefire-plugin.version>3.1.2</maven-surefire-plugin.version>
        <maven-release-plugin.version>3.0.1</maven-release-plugin.version>
        <lombok-maven-plugin.version>1.18.20.0</lombok-maven-plugin.version>
        <maven-compiler-plugin.version>3.11.0</maven-compiler-plugin.version>
        <maven-javadoc-plugin.version>3.5.0</maven-javadoc-plugin.version>
        <maven-deploy-plugin.version>3.1.1</maven-deploy-plugin.version>
        <maven-source-plugin.version>3.3.0</maven-source-plugin.version>
        <maven-gpg-plugin.version>3.1.0</maven-gpg-plugin.version>

        <sonar.organization>overcoded-io</sonar.organization>
    </properties>

    <repositories>
        <repository>
            <id>central</id>
            <url>https://repo.maven.apache.org/maven2</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>Vaadin Directory</id>
            <url>https://maven.vaadin.com/vaadin-addons</url>
        </repository>
    </repositories>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.vaadin</groupId>
                <artifactId>vaadin-bom</artifactId>
                <type>pom</type>
                <scope>import</scope>
                <version>${vaadin.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>com.vaadin</groupId>
            <artifactId>vaadin-core</artifactId>
        </dependency>
        <dependency>
            <groupId>org.hibernate.validator</groupId>
            <artifactId>hibernate-validator</artifactId>
            <version>8.0.0.Final</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>${lombok.version}</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>${jacoco-maven-plugin.version}</version>
                <executions>
                    <execution>
                        <id>prepare-agent</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>report</id>
                        <goals>
                            <goal>report</goal>
                        </goals>
                        <configuration>
                            <formats>
                                <format>XML</format>
                            </formats>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>${maven-surefire-plugin.version}</version>
                <configuration>
                    <excludes>
                        <exclude>**/*IntegrationTest.java</exclude>
                    </excludes>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-failsafe-plugin</artifactId>
                <version>${maven-failsafe-plugin.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>integration-test</goal>
                            <goal>verify</goal>
                        </goals>
                        <configuration>
                            <includes>
                                <include>**/*IntegrationTest.java</include>
                            </includes>
                        </configuration>
                    </execution>
                </executions>
                <configuration>
                    <classesDirectory>${project.build.outputDirectory}</classesDirectory>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>${maven-source-plugin.version}</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar-no-fork</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven-compiler-plugin.version}</version>
                <configuration>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                    <encoding>${file.encoding}</encoding>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>${maven-javadoc-plugin.version}</version>
                <configuration>
                    <sourcepath>${project.build.directory}/generated-sources/delombok</sourcepath>
                    <additionalOptions>-Xdoclint:none</additionalOptions>
                    <encoding>${file.encoding}</encoding>
                </configuration>
                <executions>
                    <execution>
                        <id>attach-javadocs</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-deploy-plugin</artifactId>
                <version>${maven-deploy-plugin.version}</version>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>${maven-release-plugin.version}</version>
            </plugin>
            <plugin>
                <groupId>org.eclipse.jetty</groupId>
                <artifactId>jetty-maven-plugin</artifactId>
                <version>${jetty.version}</version>
                <configuration>
                    <!-- Use test scope because the UI/demo classes are in the test package. -->
                    <useTestScope>true</useTestScope>
                    <supportedPackagings>
                        <supportedPackaging>jar</supportedPackaging>
                    </supportedPackagings>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>production</id>
            <properties>
                <vaadin.productionMode>true</vaadin.productionMode>
            </properties>
            <dependencies>
                <dependency>
                    <groupId>com.vaadin</groupId>
                    <artifactId>flow-server-production-mode</artifactId>
                    <version>${flow-server-production-mode.version}</version>
                </dependency>
            </dependencies>

            <build>
                <plugins>
                    <plugin>
                        <groupId>org.projectlombok</groupId>
                        <artifactId>lombok-maven-plugin</artifactId>
                        <version>${lombok-maven-plugin.version}</version>
                        <configuration>
                            <sourceDirectory>${project.basedir}/src/main/java</sourceDirectory>
                            <addOutputDirectory>false</addOutputDirectory>
                            <encoding>${file.encoding}</encoding>
                        </configuration>
                        <executions>
                            <execution>
                                <phase>generate-sources</phase>
                                <goals>
                                    <goal>delombok</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>com.vaadin</groupId>
                        <artifactId>vaadin-maven-plugin</artifactId>
                        <version>${vaadin.version}</version>
                        <executions>
                            <execution>
                                <goals>
                                    <goal>prepare-frontend</goal>
                                    <goal>build-frontend</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>deploy</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <version>${maven-gpg-plugin.version}</version>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>gitlab</id>
            <distributionManagement>
                <repository>
                    <id>gitlab-maven</id>
                    <url>https://gitlab.com/api/v4/projects/${projectId}/packages/maven</url>
                </repository>
            </distributionManagement>
        </profile>
        <profile>
            <id>central</id>
            <distributionManagement>
                <repository>
                    <id>ossrh</id>
                    <url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
                </repository>
            </distributionManagement>
        </profile>
    </profiles>
</project>
